package com.example.statemachine.spring;

import com.example.statemachine.domain.entity.CreditOrder;
import com.example.statemachine.domain.enums.CreditOrderEvent;
import com.example.statemachine.domain.enums.CreditOrderStatus;
import com.example.statemachine.domain.state.spring.service.CreditOrderService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.statemachine.StateMachine;

import javax.annotation.Resource;
import java.util.Random;

@SpringBootTest
public class CreditOrderTest {
    @Autowired
    private StateMachine<CreditOrderStatus, CreditOrderEvent> stateMachine;
    @Resource
    private CreditOrderService creditOrderService;
    @BeforeEach
    public void setUp() {
        stateMachine.start();
    }
    @Test
    public void test(){
        //创建授信订单
        CreditOrder creditOrder = new CreditOrder(new Random().nextInt(), CreditOrderStatus.INIT, "测试订单");
        //提交审核
        creditOrderService.processApplyAudit();
        //审批通过
        creditOrderService.processApprovalAudit();
        //拒绝审核
        creditOrderService.processRejectAudit();
        //取消
        creditOrderService.processCancel();
    }
}
