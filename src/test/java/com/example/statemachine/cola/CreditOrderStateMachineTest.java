package com.example.statemachine.cola;

import com.example.statemachine.domain.entity.CreditOrder;
import com.example.statemachine.domain.enums.CreditOrderAction;
import com.example.statemachine.domain.enums.CreditOrderStatus;
import com.example.statemachine.domain.state.cola.statemachine.CreditOrderContext;
import com.example.statemachine.domain.state.cola.statemachine.CreditOrderStateMachineFactory;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.Random;

@SpringBootTest
public class CreditOrderStateMachineTest {
    @Resource
    private CreditOrderStateMachineFactory creditOrderStateMachineFactory;
    @Test
    public void test() {
        //创建授信订单
        CreditOrder creditOrder = new CreditOrder(new Random().nextInt(), CreditOrderStatus.INIT, "测试授信单");
        CreditOrderContext creditOrderContext = new CreditOrderContext();
        creditOrderContext.setCreditOrder(creditOrder);
        //提交授信审核
        creditOrderStateMachineFactory.getStateMachine().fireEvent(creditOrder.getOrderStatus(),
                CreditOrderAction.APPLY_AUDIT,creditOrderContext);
        System.out.println("----------------------------------------");
        //授信审核通过
        creditOrderStateMachineFactory.getStateMachine().fireEvent(creditOrder.getOrderStatus(),
                CreditOrderAction.APPROVAL_AUDIT,creditOrderContext);

    }
}
