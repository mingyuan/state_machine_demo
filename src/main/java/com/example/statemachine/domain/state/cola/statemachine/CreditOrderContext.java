package com.example.statemachine.domain.state.cola.statemachine;

import com.example.statemachine.domain.entity.CreditOrder;
import lombok.Data;

@Data
public class CreditOrderContext {
    private CreditOrder creditOrder;
}
