package com.example.statemachine.domain.state.cola.statemachine;

import com.alibaba.cola.statemachine.StateMachine;

/**
 * 状态机抽象工厂
 * @param <S>
 * @param <E>
 * @param <C>
 */
public abstract class AbstractStateMachineFactory<S,E,C> implements StateMachineFactory<S,E,C> {
    protected abstract void init();
    protected abstract String getMachineId();
    @Override
    public StateMachine<S, E, C> getStateMachine() {
        return com.alibaba.cola.statemachine.StateMachineFactory.get(getMachineId());
    }
}
