package com.example.statemachine.domain.state.cola.handler;

import com.example.statemachine.domain.enums.CreditOrderAction;
import com.example.statemachine.domain.enums.CreditOrderEvent;
import com.example.statemachine.domain.enums.CreditOrderStatus;
import com.example.statemachine.domain.state.cola.statemachine.BusinessHandler;
import com.example.statemachine.domain.state.cola.statemachine.CreditOrderContext;
import org.springframework.stereotype.Component;

@Component("applyAuditHandler")
public class ApplyAuditHandler implements BusinessHandler<CreditOrderStatus, CreditOrderAction, CreditOrderContext> {
    @Override
    public void execute(CreditOrderStatus from, CreditOrderStatus to, CreditOrderAction creditOrderEvent, CreditOrderContext creditOrderContext) {
        //逻辑执行
        System.out.println("申请审核逻辑执行");
        creditOrderContext.getCreditOrder().setOrderStatus(CreditOrderStatus.AUDITING);
        System.out.println("授信单状态更新为：" + creditOrderContext.getCreditOrder().getOrderStatus());
    }

    @Override
    public boolean isSatisfied(CreditOrderContext creditOrderContext) {
        //校验提交审核的前置条件是否满足
        System.out.println("校验，提交审核，前置条件是否满足");
        return true;
    }
}
