package com.example.statemachine.domain.state.cola.handler;

import com.example.statemachine.domain.enums.CreditOrderAction;
import com.example.statemachine.domain.enums.CreditOrderStatus;
import com.example.statemachine.domain.state.cola.statemachine.BusinessHandler;
import com.example.statemachine.domain.state.cola.statemachine.CreditOrderContext;
import org.springframework.stereotype.Component;

/**
 * 审核通过业务操作
 */
@Component("approvalAuditHandler")
public class ApprovalAuditHandler implements BusinessHandler<CreditOrderStatus, CreditOrderAction, CreditOrderContext> {
    @Override
    public void execute(CreditOrderStatus creditOrderStatus, CreditOrderStatus s1, CreditOrderAction creditOrderAction, CreditOrderContext context) {
        //审核通过
        System.out.println("审核通过逻辑执行");
        context.getCreditOrder().setOrderStatus(CreditOrderStatus.PASSED);
        System.out.println("授信单状态更新为：" + context.getCreditOrder().getOrderStatus());
    }

    @Override
    public boolean isSatisfied(CreditOrderContext context) {
        //审核通过前提条件判断
        System.out.println("校验，审核通过,前置条件是否满足");
        return true;
    }
}
