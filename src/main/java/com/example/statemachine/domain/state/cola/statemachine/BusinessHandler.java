package com.example.statemachine.domain.state.cola.statemachine;

import com.alibaba.cola.statemachine.Action;
import com.alibaba.cola.statemachine.Condition;

public interface BusinessHandler<S,E,C> extends Condition<C>, Action<S,E,C> {

}
