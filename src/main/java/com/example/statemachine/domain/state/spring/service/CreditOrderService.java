package com.example.statemachine.domain.state.spring.service;

import com.example.statemachine.domain.enums.CreditOrderEvent;
import com.example.statemachine.domain.enums.CreditOrderStatus;
import org.springframework.statemachine.StateMachine;
import org.springframework.stereotype.Service;

@Service
public class CreditOrderService {
    private final StateMachine<CreditOrderStatus, CreditOrderEvent> stateMachine;
    public CreditOrderService(StateMachine<CreditOrderStatus, CreditOrderEvent> stateMachine) {
        this.stateMachine = stateMachine;
    }

    /**
     * 申请审核流程处理，发布事件
     */
    public void processApplyAudit() {
        stateMachine.sendEvent(CreditOrderEvent.AUDIT_APPLIED);
    }

    /**
     * 审核通过处理，发布事件
     */
    public void processApprovalAudit() {
        stateMachine.sendEvent(CreditOrderEvent.AUDIT_APPROVED);
    }

    /**
     * 审核拒绝处理，发布事件
     */
    public void processRejectAudit() {
        stateMachine.sendEvent(CreditOrderEvent.AUDIT_REJECTED);
    }

    /**
     * 取消处理，发布事件
     */
    public void processCancel() {
        stateMachine.sendEvent(CreditOrderEvent.CANCELED);
    }
}
