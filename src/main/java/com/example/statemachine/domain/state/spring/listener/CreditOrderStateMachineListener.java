package com.example.statemachine.domain.state.spring.listener;

import org.springframework.statemachine.annotation.OnTransition;
import org.springframework.statemachine.annotation.WithStateMachine;
import org.springframework.stereotype.Component;

@Component
@WithStateMachine
public class CreditOrderStateMachineListener {
    @OnTransition(target = "INIT")
    public void onOrderCreated() {
        System.out.println("授信单已创建");
        //初始化授信单状态，持久化存储
    }
    @OnTransition(target = "AUDITING")
    public void onOrderAuditing() {
        System.out.println("授信单已提交审核");
        //更新授信单状态，通知审批人员，等待审核结果
    }
    @OnTransition(target = "PASSED")
    public void onOrderPassed() {
        System.out.println("授信单审批通过");
        //更新授信单状态，通知客户
    }
    @OnTransition(target = "REJECTED")
    public void onOrderRejected() {
        System.out.println("授信单审批拒绝");
        //更新授信单状态，通知客户
    }
    @OnTransition(target = "CANCELLED")
    public void onOrderCancelled() {
        System.out.println("授信单已取消");
        //更新授信单状态，通知客户
    }
}
