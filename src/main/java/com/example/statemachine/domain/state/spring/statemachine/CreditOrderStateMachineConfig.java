package com.example.statemachine.domain.state.spring.statemachine;

import com.example.statemachine.domain.enums.CreditOrderEvent;
import com.example.statemachine.domain.enums.CreditOrderStatus;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachine;
import org.springframework.statemachine.config.EnumStateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

@Configuration
@EnableStateMachine
public class CreditOrderStateMachineConfig extends EnumStateMachineConfigurerAdapter<CreditOrderStatus, CreditOrderEvent> {

    @Override
    public void configure(StateMachineStateConfigurer<CreditOrderStatus, CreditOrderEvent> states) throws Exception {
        states.withStates().initial(CreditOrderStatus.INIT)
                .state(CreditOrderStatus.INIT)
                .state(CreditOrderStatus.AUDITING)
                .state(CreditOrderStatus.PASSED)
                .state(CreditOrderStatus.REJECTED)
                .state(CreditOrderStatus.CANCELLED);
    }
    @Override
    public void configure(StateMachineTransitionConfigurer<CreditOrderStatus, CreditOrderEvent> transitions) throws Exception {
        transitions.withExternal()
                .source(CreditOrderStatus.INIT).target(CreditOrderStatus.AUDITING).event(CreditOrderEvent.AUDIT_APPLIED)//从初始状态到审核中，触发了提交审核事件
                .and().withExternal()
                .source(CreditOrderStatus.AUDITING).target(CreditOrderStatus.PASSED).event(CreditOrderEvent.AUDIT_APPROVED)//从审核中到通过，触发了审核通过事件
                .and().withExternal()
                .source(CreditOrderStatus.AUDITING).target(CreditOrderStatus.REJECTED).event(CreditOrderEvent.AUDIT_REJECTED)//从审核中到拒绝，触发了审核拒绝事件
                .and().withExternal()
                .source(CreditOrderStatus.REJECTED).target(CreditOrderStatus.CANCELLED).event(CreditOrderEvent.CANCELED);//从拒绝到取消，触发了取消授信事件
    }


}
