package com.example.statemachine.domain.state.cola.statemachine;

import com.alibaba.cola.statemachine.StateMachine;

public interface StateMachineFactory<S,E,C> {
    /**
     * 获取状态机对象
     * @return
     */
    StateMachine<S,E,C> getStateMachine();
}
