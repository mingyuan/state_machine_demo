package com.example.statemachine.domain.entity;

import com.example.statemachine.domain.enums.CreditOrderStatus;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CreditOrder {
    private Integer orderId;
    private CreditOrderStatus orderStatus;
    private String orderName;
}
