package com.example.statemachine.domain.enums;
/**
 * 授信订单行为枚举
 */
public enum CreditOrderAction {
    APPLY_AUDIT,//申请审核
    APPROVAL_AUDIT,//审核通过
    REJECT_AUDIT,//审核拒绝
    CANCEL //取消
}
