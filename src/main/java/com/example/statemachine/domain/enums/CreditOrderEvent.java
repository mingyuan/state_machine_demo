package com.example.statemachine.domain.enums;

public enum CreditOrderEvent {
    AUDIT_APPLIED,//申请审核
    AUDIT_APPROVED,//审核通过
    AUDIT_REJECTED,//审核拒绝
    CANCELED //取消
}
