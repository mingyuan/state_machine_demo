package com.example.statemachine.domain.enums;

public enum CreditOrderStatus {
    INIT,
    AUDITING,
    PASSED,
    REJECTED,
    CANCELLED
}
